<?php 
namespace App\Models;

use App\Models\Types\Quality;
use App\Models\Types\SellIn;

class BaseProduct {
    public $sellIn, $quality, $name;

    CONST defaultName = 'normal',
        piscoPeruano = 'Pisco Peruano',    
        tumi = 'Tumi de Oro Moche',
        ticketVip = 'Ticket VIP al concierto de Pick Floid',
        cafe = 'Café Altocusco';

    /**
     * @param int $sellIn 
     * @param int $quality 
     * @return void 
     */
    public function __construct(int $sellIn, int $quality)
    {
        $this->sellIn = new SellIn($sellIn);
        $this->quality = new Quality($quality);
    }

    /** @return void  */
    public function execute(){
        $this->updateQuality();
        $this->updateSellIn();
        $this->updateFinalStatus();
    }

    /** @return void  */
    public function updateSellIn() { 
        $this->sellIn->decrement();
    }

    /** @return void  */
    public function updateQuality() {
        $this->quality->decrement();
    }

    /** @return void  */
    public function updateFinalStatus(){
        if($this->sellIn->isExpired()){
            $this->quality->decrement();
        }
    }

}