<?php 
namespace App\Models;

use Exception;

/** @package App\Models */
class LegendaryProduct extends BaseProduct{

    /** @return void  */
    public function execute(){
        $this->quality->setMaxValue(80);
    }

    /**
     * @return void 
     * @throws Exception 
     */
    public function updateSellIn() { 
        throw new Exception('Un producto legendario, nunca debe ser vendido');
    }

    /** @return void  */
    public function updateQuality() {
        throw new Exception('El quality de un producto legandario nunca cambia');
    }

    /** @return void  */
    public function updateFinalStatus(){
        throw new Exception('El quality de un producto legandario nunca cambia');
    }
}