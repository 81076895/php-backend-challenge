<?php 
namespace App\Models;

class TicketVip extends BaseProduct {

    /** @return void  */
    public function updateQuality() {
        $this->quality->increment();
        if($this->sellIn->getValue() < 11){
            $this->quality->increment();
        }

        if($this->sellIn->getValue() < 6){
            $this->quality->increment();
        }
    }

    /** @return void  */
    public function updateFinalStatus()
    {
        if($this->sellIn->isExpired()){
            $this->quality->decrement($this->quality->getValue());
        }
    }
}