<?php 
namespace App\Models;

class PiscoPeruano extends BaseProduct {

    /** @return void  */
    public function updateQuality() {
        $this->quality->increment();
    }

    /** @return void  */
    public function updateFinalStatus()
    {
        if($this->sellIn->isExpired()){
            $this->quality->increment();
        }
    }

}