<?php 
namespace App\Models;

class Cafe extends BaseProduct {

    /** @return void  */
    public function updateQuality() {
        $this->quality->decrement(2);
    }

    /** @return void  */
    public function updateFinalStatus()
    {
        if($this->sellIn->isExpired()){
            $this->quality->decrement(2);
        }
    }

}