<?php
namespace App\Models\Types;

use App\Models\Types\IntegerType;

class SellIn extends IntegerType
{   
    /** @return bool  */
    public function isExpired():bool {
        return $this->getValue() < 0;
    }
}
