<?php
namespace App\Models\Types;

use App\Models\Types\IntegerType;

class Quality extends IntegerType
{   
    private $maxValue = 50, $minValue = 0;

    /**
     * @param int $maxValue 
     * @return void 
     */
    public function setMaxValue(int $maxValue){
        $this->maxValue = $maxValue;
    }

    /** @return int  */
    public function getMaxValue(): int {
        return $this->maxValue;
    }

    /**
     * @param int $minValue 
     * @return void 
     */
    public function setMinValue(int $minValue){
        $this->minValue = $minValue;
    }

    /** @return int  */
    public function getMinValue(): int {
        return $this->minValue;
    }

    /**
     * @param int $decrementValue 
     * @return void 
     */
    public function decrement(int $decrementValue = 1){
        if($this->getValue() > $this->getMinValue()){
            $this->modify(-$decrementValue);
        }
    }

    /**
     * @param int $incrementValue 
     * @return void 
     */
    public function increment(int $incrementValue = 1){
        if($this->getValue() < $this->getMaxValue()){
            $this->modify(+$incrementValue);
        }   
    }
}
