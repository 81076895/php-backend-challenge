<?php

namespace App\Models\Types;

abstract class IntegerType
{
    protected $value;

    /**
     * @param string $name 
     * @param int $value 
     * @return void 
     */
    function __construct(int $value)
    {   
        $this->value = $value;
    }

    /** @return int  */
    function getValue(): int{
        return $this->value;
    }

    /**
     * @param int $modifyValue 
     * @return void 
     */
    function modify(int $modifyValue){
        $this->value += $modifyValue;
    }

    /**
     * @param int $incrementValue 
     * @return void 
     */
    function increment(int $incrementValue = 1){
        $this->modify(+$incrementValue);
    }

    /**
     * @param int $decrementValue 
     * @return void 
     */
    function decrement(int $decrementValue = 1){
        $this->modify(-$decrementValue);
    }
}
