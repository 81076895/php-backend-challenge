<?php 

namespace App\Factories;

use App\Models\BaseProduct;
use App\Models\Cafe;
use App\Models\PiscoPeruano;
use App\Models\TicketVip;
use App\Models\Tumi;

class ProductFactory
{
    /**
     * @param string $name 
     * @param int $sellIn 
     * @param int $quality 
     * @return BaseProduct 
     */
    public static function init(string $name, int $sellIn, int $quality): BaseProduct
    {
        switch($name){
            case BaseProduct::piscoPeruano:
                return new PiscoPeruano($sellIn, $quality);
            
            case BaseProduct::tumi:
                return new Tumi($sellIn, $quality);
            
            case BaseProduct::ticketVip:
                return new TicketVip($sellIn, $quality);
        
            case BaseProduct::cafe:
                return new Cafe($sellIn, $quality);
            
            default:
                return new BaseProduct($sellIn, $quality);
        }
    }
}