<?php

namespace App;

use App\Factories\ProductFactory;
use App\Models\BaseProduct;

class VillaPeruana
{
    /** @var BaseProduct */
    private $product;
    public $sellIn, $quality;

    /**
     * @param string $name 
     * @param int $quality 
     * @param int $sellIn 
     * @return void 
     */
    public function __construct(string $name, int $quality, int $sellIn)
    {
        $this->product = ProductFactory::init($name, $sellIn, $quality);
    }

    /**
     * @param string $name 
     * @param int $quality 
     * @param int $sellIn 
     * @return static 
     */
    public static function of(string $name, int $quality, int $sellIn) {
        return new static($name, $quality, $sellIn);
    }

    /** @return void  */
    public function tick(){
        $this->product->execute();
        $this->sellIn = $this->product->sellIn->getValue();
        $this->quality = $this->product->quality->getValue();
    }
}
